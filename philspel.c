/*
 * Include the provided hashtable library
 */
#include "hashtable.h"

/*
 * Include the header file
 */
#include "philspel.h"

/*
 * Standard IO and file routines
 */
#include <stdio.h>

/*
 * General utility routines (including malloc())
 */
#include <stdlib.h>

/*
 * Character utility routines.
 */
#include <ctype.h>

/*
 * String utility routines
 */
#include <string.h>

/*
 * Math utility routines
 */
#include <math.h>

/*
 * this hashtable stores the dictionary
 */
HashTable *dictionary;
void check();
/*
 * the MAIN routine.  You can safely print debugging information
 * to standard error (stderr) and it will be ignored in the grading
 * process, in the same way which this does.
 */
int main(int argc, char **argv){
    if(argc != 2){
        fprintf(stderr, "Specify a dictionary\n");
        return 0;
    }
    /*
     * Allocate a hash table to store the dictionary
     */
    fprintf(stderr, "Creating hashtable\n");
    dictionary = createHashTable(2255, &stringHash, &stringEquals);
    
    fprintf(stderr, "Loading dictionary %s\n", argv[1]);
    readDictionary(argv[1]);
    fprintf(stderr, "Dictionary loaded\n");
    
    fprintf(stderr, "Processing stdin\n");
    processInput();
    
    /* main in C should always return 0 as a way of telling
     whatever program invoked this that everything went OK
     */
    return 0;
}

/*
 * You need to define this function. void *s can be safely casted
 * to a char * (null terminated string) which is done for you here for
 * convenience.
 */
unsigned int stringHash(void *s){
    char *string = (char *) s;
    int n = strlen(string);
    unsigned int result = 0;
    int index = 0;
    for (; index < n; index++) {
        result = string[index] + 31*result;
    }
    return result;
}

/*
 * You need to define this function.  It should return a nonzero
 * value if the two strings are identical (case sensitive comparison)
 * and 0 otherwise.
 */
int stringEquals(void *s1, void *s2){
    char *string1 = (char *) s1;
    char *string2 = (char *) s2;
    
    while (*string1 && *string2 && (*string1 == *string2)) {
        string1++;
        string2++;
    }
    return !(*string1 - *string2);
}

/*
 * this function should read in every word (sequence of letters unbroken
 * by any non-letter character) in the dictionary and
 * store it in the dictionary.  You should first open the file specified,
 * then read the words one at a time and insert them into the dictionary.
 * Once the file is read in completely, exit.  You will need to allocate
 * (using malloc()) space for each word.  As described in the specs, you
 * can initially assume that no word is longer than 60 characters.  However,
 * for the final 20% of your grade, you cannot assumed that words have a bounded
 * length You can NOT assume that the specified file exists.  If the file does
 * NOT exist, you should print some message to standard error and call exit(0)
 * to cleanly exit the program. Since the format is one word at a time, with
 * returns in between, you can
 * safely use fscanf() to read in the strings.
 */
void readDictionary(char *filename){
    FILE *input = fopen(filename, "r");
    unsigned index = 0;
    unsigned size = 8;
    char c;
    char* word = (char*)malloc(size*sizeof(char));
    
    if (input) {
        while ((c = getc(input)) != EOF){
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                if (index >= size-1) {
                    size *= 2;
                    word = (char*)realloc(word, size*sizeof(char));
                }
                word[index++] = c;
            } else {
                word[index] = '\0';
                insertData(dictionary, (void*)word, (void*)word);
                index = 0;
                size = 8;
                word = (char*)malloc(size*sizeof(char));
            }
        }
        free((void*)word);
        fclose(input);
    } else {
        fprintf(stderr,"Error opening file\n");
        exit(0);
    }
}


/*
 * This should process standard input and copy it to standard output
 * as specified in specs.  EG, if a standard dictionary was used
 * and the string "this is a taest of  this-proGram" was given to
 * standard input, the output to standard output (stdout) should be
 * "this is a teast [sic] of  this-proGram".  All words should be checked
 * against the dictionary as they are input, again with all but the first
 * letter converted to lowercase, and finally with all letters converted
 * to lowercase.  Only if all 3 cases are not in the dictionary should it
 * be reported as not being found, by appending " [sic]" after the
 * error.
 *
 * Since we care about preserving whitespace, and pass on all non alphabet
 * characters untouched, and with all non alphabet characters acting as
 * word breaks, scanf() is probably insufficent (since it only considers
 * whitespace as breaking strings), so you will probably have
 * to get characters from standard input one at a time.
 *
 * As stated in the specs, you can initially assume that no word is longer than
 * 60 characters, but you may have strings of non-alphabetic characters (eg,
 * numbers, punctuation) which are longer than 60 characters. For the final 20%
 * of your grade, you can no longer assume words have a bounded length.
 */
void processInput(){
    unsigned index = 0;
    unsigned size = 8;
    char c;
    char* word = (char*)malloc(size*sizeof(char));
    
    
    while (1){
        c = getc(stdin);
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            if (index >= size-1) {
                size *= 2;
                word = (char*)realloc(word, size*sizeof(char));
            }
            word[index++] = c;
        } else {
            word[index] = '\0';
            fprintf(stdout, "%s", word);
            if (word[0] != '\0' && word[1] != '\0' && !findData(dictionary, word)) {
                char* copy;
                int i = 1;
                copy = (char*)malloc(strlen(word)+1);
                copy[0] = word[0];
                for (; i <= strlen(word); i++) {
                    copy[i] = tolower(word[i]);
                }
                if(!findData(dictionary, copy)) {
                    copy[0] = tolower(copy[0]);
                    if (!findData(dictionary, copy)) {
                        fprintf(stdout, " [sic]");
                        word[index] = c;
                        
                    }
                }
                free((void*)copy);
            }
            if (c == EOF) {
                free((void*)word);
                break;
            }
            fprintf(stdout, "%c", c);
            index = 0;
            size = 8;
            free((void*)word);
            word = (char*)malloc(size*sizeof(char));
        }
    }
}
